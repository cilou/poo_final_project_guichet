package poo_final.business_logic;

import poo_final.data.Display;
import poo_final.data.DisplayStatus;

public class DisplayController {

    private Display display;

    public DisplayController(Display display) {
        this.setDisplay(display);
    }

    private void setDisplay(Display display) {
        this.display = display;
    }

    public Display getDisplay() {
        return this.display;
    }

    public void setDisplayStatus(DisplayStatus status) {
        this.display.setStatus(status);
    }

    public DisplayStatus getDisplayStatus() {
        return this.display.getStatus();
    }

    public void updateDisplay() {
        for(DeskController desk : this.display.getDeskListController().getDeskList()) {
            System.out.print("Desk ID: " + desk.getDeskId() + "\t" +
                    "STATUS: " + desk.getDeskStatus() + "\t");
            if(desk.getClient() != null && desk.getClient().getClientId() != 0) {
                System.out.println("CLIENT: " + desk.getClient().getClientId());
            } else {
                System.out.println("CLIENT: ---");
            }
        }
    }
}
