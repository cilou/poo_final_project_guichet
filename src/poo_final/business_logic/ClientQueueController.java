package poo_final.business_logic;

import poo_final.data.Client;
import poo_final.data.ClientQueue;
import poo_final.data.DeskStatus;
import poo_final.presentation.interfaces.IClientQueueListener;
import poo_final.presentation.interfaces.IDeskListListener;
import poo_final.presentation.interfaces.ITicketStationListener;

import javax.swing.event.EventListenerList;
import java.util.ArrayList;
import java.util.List;

public class ClientQueueController implements ITicketStationListener, IDeskListListener {

    private ClientQueue clientQueue;

    public ClientQueueController(ClientQueue clientQueue) {
        this.clientQueue = clientQueue;
    }

    public int getNumberOfClients() {
        return clientQueue.getQueueSize();
    }

    public List<ClientController> getClientList() {
        ClientController[] clients = this.clientQueue.getClients();
        List<ClientController> clientControllers = new ArrayList<>();
        for(ClientController client : clients) {
            clientControllers.add(client);
        }
        return clientControllers;
    }

    // ======================
    // Gestion des listeners
    // ======================
    private EventListenerList listeners = new EventListenerList();

    public void addClientQueueListener(IClientQueueListener listener) {
        listeners.add(IClientQueueListener.class, listener);
    }

    public void removeClientQueueListener(IClientQueueListener listener) {
        listeners.remove(IClientQueueListener.class, listener);
    }

    private IClientQueueListener[] getClientQueueListeners() {
        return listeners.getListeners(IClientQueueListener.class);
    }

    private void fireAddedClientToQueue(ClientController client) {
        for(IClientQueueListener listener : this.getClientQueueListeners()) {
            listener.onNewClient(client);
        }
    }

    // ======================
    // Gestion des événements
    // ======================

    @Override
    public void onNewTicket(int ticketId) {
        System.out.println("ClientQueueController -- Received signal onNewTicket from TicketStationController");
        System.out.println("ClientQueueController -- add new client id " + ticketId + " to the queue");
        ClientController newClient = new ClientController(new Client(ticketId));
        this.clientQueue.addClientToQueue(newClient);
        System.out.println("ClientQueueController -- fire fireAddedClientToQueue");
        this.fireAddedClientToQueue(newClient);
    }

    @Override
    public void onOpenDeskAvailable(DeskController desk) {
        System.out.println("ClientQueueController -- Received signal onOpenDeskAvailable from DeskListController");
        if(this.getNumberOfClients() <= 0) {
            System.out.println("ClientQueueController -- ClientQueue is empty, no client to assign to the available desk");
            return;
        }
        System.out.println("ClientQueueController -- ClientQueue is not empty, assigning head of queue to desk id "
                + desk.getDeskId());
        desk.setClient(this.clientQueue.removeClientFromQueue());
        System.out.println("ClientQueueController -- Client id " + desk.getClient().getClientId() +
                " assigned to desk id " + desk.getDeskId());
        desk.setDeskStatus(DeskStatus.WAITING_FOR_CLIENT); // must be set by DeskController when signal received ??
    }

    //UNUSED
    @Override
    public void onDeskListChange(DeskController desk, int oldCount, int newCount) { }
}
