package poo_final.business_logic;

import poo_final.data.TicketStation;
import poo_final.data.TicketStationStatus;
import poo_final.exceptions.TicketStationException;
import poo_final.presentation.interfaces.IDeskListListener;
import poo_final.presentation.interfaces.ITicketStationListener;

import javax.swing.event.EventListenerList;

public class TicketStationController implements IDeskListListener {

    private TicketStation ticketStation;

    public TicketStationController(TicketStation ticketStation) {
        this.setTicketStation(ticketStation);
    }

    public TicketStation getTicketStation() {
        return ticketStation;
    }

    private void setTicketStation(TicketStation ticketStation) {
        this.ticketStation = ticketStation;
    }

    private void switchTicketStationOn() {
        this.ticketStation.setStatus(TicketStationStatus.ON);
        System.out.println("TicketStationController -- TicketStation switched ON");
    }

    private void switchTicketStationOff() {
        this.ticketStation.setStatus(TicketStationStatus.OFF);
        System.out.println("TicketStationController -- TicketStation switched OFF");
    }

    public TicketStationStatus getSTicketStationStatus() {
        return this.ticketStation.getStatus();
    }

    public int getLastTicketIdIssued() {
        return this.ticketStation.getLastTicketIdIssued();
    }

    public void newTicket() throws TicketStationException {
        int ticketId = this.ticketStation.getTicket();
        this.fireNewTicketIssued(ticketId);
    }

    // ======================
    // Gestion des listeners
    // ======================
    private final EventListenerList listeners = new EventListenerList();

    public void addTicketStationListener(ITicketStationListener listener) {
        listeners.add(ITicketStationListener.class, listener);
    }

    public void removeTicketStationListener(ITicketStationListener listener) {
        listeners.remove(ITicketStationListener.class, listener);
    }

    private ITicketStationListener[] getTicketStationListeners() {
        return listeners.getListeners(ITicketStationListener.class);
    }

    private void fireNewTicketIssued(int ticketId) {
        for(ITicketStationListener listener : getTicketStationListeners()) {
            listener.onNewTicket(ticketId);
        }
    }

    // ======================
    // Gestion des événements
    // ======================
    @Override
    public void onDeskListChange(DeskController desk, int oldCount, int newCount) {
        if(newCount <= 0)
            this.switchTicketStationOff();
        else if(this.getSTicketStationStatus() == TicketStationStatus.OFF)
            this.switchTicketStationOn();
    }

    // UNUSED
    @Override
    public void onOpenDeskAvailable(DeskController desk) {}
}
