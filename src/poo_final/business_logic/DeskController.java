package poo_final.business_logic;

import poo_final.data.ClientStatus;
import poo_final.data.Desk;
import poo_final.data.DeskStatus;
import poo_final.presentation.interfaces.IClientListener;
import poo_final.presentation.interfaces.IDeskListener;

import javax.swing.event.EventListenerList;

public class DeskController implements IClientListener {

    private Desk desk;

    public DeskController(Desk desk, DeskListController deskListController) {
        this.desk = desk;
        this.addDeskListener(deskListController);
    }

    public void setDeskStatus(DeskStatus deskStatus) {
        if(this.getDeskStatus() == deskStatus) return;
        this.desk.setStatus(deskStatus);
        if(deskStatus == DeskStatus.OPEN) {
            this.desk.setClient(null);
        }
        //System.out.println("DeskController for desk id " + this.getDeskId() + " set status to " + this.getDeskStatus());

        this.fireDeskStatusChanged(this);

        if(deskStatus == DeskStatus.WAITING_FOR_CLIENT) {
            ClientController clientController = this.getClient();
            // Only this desk controller listens to this particular client controller ;-)
            // We prepare to listen for the client status changes to know if the client will come
            // at the desk to be served or not
            clientController.addClientListener(this);
            // We call the client to the desk
            clientController.calledToDesk(desk.getId());
        }
    }

    public DeskStatus getDeskStatus() {
        return this.desk.getStatus();
    }

    public int getNbClientsServed() {
        return this.desk.getNbClientsServed();
    }

    private void setNbClientsServed(int number) {
        this.desk.setNbClientsServed(number);
    }

    private void incrementNbClientsServed() {
        this.setNbClientsServed(this.getNbClientsServed() + 1);
    }

    public int getDeskId() {
        return this.desk.getId();
    }

    public Desk getDesk() {
        return this.desk;
    }

    public void setClient(ClientController client) {
        this.desk.setClient(client);
    }

    public ClientController getClient() {
        return this.desk.getClient();
    }

    // If the client is leaving, we suppress its reference in this desk controller's desk
    private void deleteClient() {
        this.desk.setClient(null);
    }


    // ======================
    // Gestion des listeners
    // ======================
    private EventListenerList listeners = new EventListenerList();

    public void addDeskListener(IDeskListener listener) {
        listeners.add(IDeskListener.class, listener);
    }

    public void removeDeskListener(IDeskListener listener) {
        listeners.remove(IDeskListener.class, listener);
    }

    private IDeskListener[] getDeskListeners() {
        return listeners.getListeners(IDeskListener.class);
    }

    private void fireDeskStatusChanged(DeskController desk) {
        System.out.println("DeskController  -- desk id " + desk.getDeskId() +
                " status changed to " + desk.getDeskStatus());
        for(IDeskListener listener : this.getDeskListeners()) {
            listener.onDeskStatusChanged(desk);
        }
    }

    // ======================
    // Gestion des événements
    // ======================
    @Override
    public void onClientStatusChanged(ClientController client) {
        // TODO
        /*
        if(clientController.getClientId() != this.getClient().getClientId()) {
            throw new Exception("DeskController -- onClientStatusChanged -- client ID's are different, " +
                    "this should NEVER happen!");
        }
        */
        if(this.getClient() != null) {
            System.out.println("DeskController -- received signal onClientStatusChanged from client " +
                    this.getClient().getClientId() + " with status " + this.getClient().getClientStatus() +
                    " for desk id " + this.getDeskId());
        } else {
            System.out.println("DeskController -- received signal onClientStatusChanged from no client " +
                    " for desk id " + this.getDeskId());
        }

        if(client.getClientStatus() == ClientStatus.BEING_SERVED) {
            this.incrementNbClientsServed();
            this.setDeskStatus(DeskStatus.SERVING_CLIENT);
        } else if(client.getClientStatus() == ClientStatus.LEAVING) {
            this.deleteClient();
            if(this.getDeskStatus() == DeskStatus.SERVING_CLIENT) {
                this.setDeskStatus(DeskStatus.OPEN);
            }
        }
    }
}
