package poo_final.business_logic;

import poo_final.data.DisplayList;
import poo_final.presentation.interfaces.IDeskListListener;

import java.util.ArrayList;
import java.util.List;

public class DisplayListController implements IDeskListListener {

    private DisplayList displayList;

    public DisplayListController(DisplayList displayList) {
        this.setDisplayList(displayList);
    }

    private void setDisplayList(DisplayList displayList) {
        this.displayList = displayList;
    }

    public void addDisplay(DisplayController display) {
        this.displayList.addDisplay(display);
    }

    public void removeDisplay(DisplayController display) {
        this.displayList.removeDisplay(display);
    }

    public int getNumberOfDisplays() {
        return this.displayList.getSize();
    }

    public List<DisplayController> getDisplayList() {
        DisplayController[] displays = this.displayList.getDisplays();
        List<DisplayController> displayControllers = new ArrayList<>();
        for(DisplayController display : displays) {
            displayControllers.add(display);
        }

        return displayControllers;
    }

    // ======================
    // Gestion des événements
    // ======================
    /* IDeskListListener Methods */
    @Override
    public void onDeskListChange(DeskController desk, int oldCount, int newCount) {
        System.out.println("DisplayListController -- received signal onDeskListChange from DeskListController");
        List<DisplayController> displayControllers = this.getDisplayList();
        for(DisplayController displayController : displayControllers) {
            displayController.updateDisplay();
        }
    }

    // UNUSED
    @Override
    public void onOpenDeskAvailable(DeskController desk) {

    }
}
