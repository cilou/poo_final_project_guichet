package poo_final.business_logic;

import poo_final.data.Client;
import poo_final.data.ClientStatus;
import poo_final.presentation.interfaces.IClientListener;

import javax.swing.event.EventListenerList;
import java.time.Duration;
import java.time.LocalDateTime;

public class ClientController {
    private Client client;
    private int deskId;

    public ClientController(Client client) {
        this.client = client;
        this.deskId = 0;
    }

    public Client getClient() { return this.client; }

    public int getClientId() {
        if(this.client != null) {
            return this.client.getId();
        } else {
            return 0;
        }
    }

    public ClientStatus getClientStatus() {
        return this.client.getStatus();
    }

    public void setClientStatus(ClientStatus status) {
        this.client.setStatus(status);
        this.fireClientStatusChanged(this);
    }

    public int getDeskId() {
        return this.deskId;
    }

    private void setDeskId(int deskId) {
        this.deskId = deskId;
    }

    public void calledToDesk(int deskId) {
        this.setDeskId(deskId);
        this.setClientStatus(ClientStatus.BEING_CALLED);

        // This will be handled by the GUI, just for tests purposes in the console here
        /*
        if(this.getClientId() % 2 == 0) {
            this.setClientStatus(ClientStatus.BEING_SERVED);
        } else {
            this.setClientStatus(ClientStatus.LEAVING);
        }
        */
    }

    public long getTimeElapsedSinceArrivalInSeconds() {
        LocalDateTime now = LocalDateTime.now();
        Duration timeElapsed = Duration.between(this.client.getTimeOfArrival(), now);
        return timeElapsed.getSeconds();
    }

    public String getTimeElapsedSinceArrivalFormatted() {
        long seconds = this.getTimeElapsedSinceArrivalInSeconds();
        long nbSeconds = seconds % 60;
        long remainingInMinutes = (seconds - nbSeconds) / 60;
        long nbMinutes = remainingInMinutes % 60;
        long remainingInHours = (remainingInMinutes - nbMinutes) / 60;
        long nbHours = remainingInHours;
        return String.format("%02d:%02d:%02d", nbHours, nbMinutes, nbSeconds);
    }

    @Override
    public String toString() {
        return String.format(
                "Client ID: %d\n" + "ClientStatus: %s\n" + "Client Waiting Time: %s\n",
                this.getClientId(),
                this.getClientStatus(),
                this.getTimeElapsedSinceArrivalFormatted()
        );
    }

    // ======================
    // Gestion des listeners
    // ======================
    // Not necessary to have a list here since there is only one listener per controller,
    // the desk controller, for the moment
    private EventListenerList listeners = new EventListenerList();

    public void addClientListener(IClientListener listener) {
        listeners.add(IClientListener.class, listener);
    }

    public void removeClientListener(IClientListener listener) {
        listeners.remove(IClientListener.class, listener);
    }

    private IClientListener[] getClientListeners() {
        return listeners.getListeners(IClientListener.class);
    }

    /*
     * @param client    Unnecessary since the only listener to the client is the desk controller
     *                  that "owns" it. I leave it for control purposes (to be sure that the
     *                  client's ids here and in the deskcontroller are the same
     */
    private void fireClientStatusChanged(ClientController client) {
        System.out.println("ClientController  -- client id " + client.getClientId() +
                " status changed to " + client.getClientStatus());
        for(IClientListener listener : this.getClientListeners()) {
            listener.onClientStatusChanged(client);
        }
    }
}
