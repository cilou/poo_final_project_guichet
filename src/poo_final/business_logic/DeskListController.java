package poo_final.business_logic;

import poo_final.data.DeskList;
import poo_final.data.DeskStatus;
import poo_final.presentation.interfaces.IClientQueueListener;
import poo_final.presentation.interfaces.IDeskListListener;
import poo_final.presentation.interfaces.IDeskListener;

import javax.swing.event.EventListenerList;
import java.util.ArrayList;
import java.util.List;

public class DeskListController implements IClientQueueListener, IDeskListener {

    private DeskList deskList;

    public DeskListController(DeskList deskList) {
        this.deskList = deskList;
    }

    public int getNumberOfDesks() {
        return this.deskList.getSize();
    }

    public void addDesk(DeskController desk) {
        this.deskList.addDesk(desk);
        this.fireAddedDeskToList(desk);
    }

    public void removeDesk(DeskController desk) {
        this.deskList.removeDesk(desk);
        this.fireRemovedDeskFromList(desk);
    }

    public List<DeskController> getDeskList() {
        DeskController[] desks = this.deskList.getDesks();
        List<DeskController> deskControllers = new ArrayList<>();
        for(DeskController desk : desks) {
            deskControllers.add(desk);
        }
        return deskControllers;
    }

    // ======================
    // Gestion des listeners
    // ======================
    private EventListenerList listeners = new EventListenerList();

    public void addDeskListListener(IDeskListListener listener) {
        listeners.add(IDeskListListener.class, listener);
    }

    public void removeDeskListListener(IDeskListListener listener) {
        listeners.remove(IDeskListListener.class, listener);
    }

    private IDeskListListener[] getDeskListListeners() {
        return listeners.getListeners(IDeskListListener.class);
    }

    private void fireAddedDeskToList(DeskController desk) {
        for(IDeskListListener listener : this.getDeskListListeners()) {
            listener.onDeskListChange(desk, this.getNumberOfDesks() - 1, this.getNumberOfDesks());
        }
    }

    private void fireRemovedDeskFromList(DeskController desk) {
        for(IDeskListListener listener : this.getDeskListListeners()) {
            listener.onDeskListChange(desk, this.getNumberOfDesks() + 1, this.getNumberOfDesks());
        }
    }

    private void fireOpenDeskAvailable(DeskController desk) {
        for(IDeskListListener listener : this.getDeskListListeners()) {
            listener.onOpenDeskAvailable(desk);
            //listener.onDeskListChange(desk, this.getNumberOfDesks(), this.getNumberOfDesks());
        }
    }

    private void fireDeskListChanged(DeskController desk) {
        for(IDeskListListener listener : this.getDeskListListeners()) {
            listener.onDeskListChange(desk, this.getNumberOfDesks(), this.getNumberOfDesks());
        }
    }

    // ======================
    // Gestion des événements
    // ======================
    /* IClientQueueListener Methods */
    @Override
    public void onNewClient(ClientController client) {
        System.out.println("DeskListController -- received signal onNewClient");
        DeskController openDesk = getDeskWithStatus(DeskStatus.OPEN);
        if(openDesk != null) {
            System.out.println("DeskListController onNewClient fires fireOpenDeskAvailable -- Desk id " +
                    openDesk.getDeskId() + " is OPEN");
            this.fireOpenDeskAvailable(openDesk);
        } else {
            System.out.println("DeskListController onNewClient -- no open desk available.");
        }
    }

    private DeskController getDeskWithStatus(DeskStatus status) {
        System.out.println("DeskListController -- getDeskWithStatus " + status);
        DeskController deskWithStatus = null;
        for(DeskController desk: this.getDeskList()) {
            //System.out.println("Desk id: " + desk.getDeskId() + ", desk status: " + desk.getDeskStatus());
            if(desk.getDeskStatus() == status) {
                if(deskWithStatus == null || deskWithStatus.getNbClientsServed() > desk.getNbClientsServed()) {
                    deskWithStatus = desk;
                }
            }
        }

        return deskWithStatus;
    }

    /* IDeskListener Methods */
    @Override
    public void onDeskStatusChanged(DeskController desk) {
        System.out.println("DeskListController -- Received signal onDeskStatusChanged from desk id " + desk.getDeskId());
        this.fireDeskListChanged(desk); // update displays
        if(desk.getDeskStatus() == DeskStatus.OPEN) {
            this.fireOpenDeskAvailable(desk);
        }
    }
}
