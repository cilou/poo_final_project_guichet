package poo_final.data;

import poo_final.exceptions.TicketStationException;

public class TicketStation {
    private static int nextId = 1; // to avoid duplicate ticket id's
    private TicketStationStatus status;

    public TicketStation() {
        this.setStatus(TicketStationStatus.OFF);
    }

    public int getTicket() throws TicketStationException {
        // fixme throw exception in controller instead ???
        if(this.getStatus() == TicketStationStatus.OFF) {
            throw new TicketStationException("Cannot issue tickets => Station is OFF");
        }
        return TicketStation.nextId++;
    }

    public int getLastTicketIdIssued() {
        return TicketStation.nextId - 1;
    }

    public TicketStationStatus getStatus() {
        return status;
    }

    public void setStatus(TicketStationStatus status) {
        this.status = status;
    }
}
