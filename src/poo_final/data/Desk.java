package poo_final.data;

import poo_final.business_logic.ClientController;

public class Desk {
    private static int nextId = 1;

    private int id;
    private int nbClientsServed;
    private DeskStatus status;
    private ClientController client;

    public Desk() {
        this.setId(nextId++);
        this.setStatus(DeskStatus.CLOSED);
        this.setNbClientsServed(0);
    }

    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    public void setNbClientsServed(int number) {
        this.nbClientsServed = number;
    }

    public int getNbClientsServed() {
        return this.nbClientsServed;
    }

    public DeskStatus getStatus() {
        return status;
    }

    public void setStatus(DeskStatus status) {
        this.status = status;
    }

    public ClientController getClient() {
        return client;
    }

    public void setClient(ClientController client) {
        this.client = client;
    }


    @Override
    public String toString() {
        return String.format(
                "Desk ID: %d\n" + "DeskStatus: %s\n",
                this.getId(), this.getStatus()
        );
    }
}
