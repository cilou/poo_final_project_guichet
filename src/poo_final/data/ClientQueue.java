package poo_final.data;

import poo_final.business_logic.ClientController;

import java.util.concurrent.LinkedBlockingQueue;

public class ClientQueue {

    private LinkedBlockingQueue<ClientController> clientQueue;

    public ClientQueue() {
        this.clientQueue = new LinkedBlockingQueue();
    }

    public void addClientToQueue(ClientController client) {
        this.clientQueue.add(client);
    }

    public ClientController removeClientFromQueue() {
        return this.clientQueue.poll();
    }

    public ClientController[] getClients() {
        ClientController[] clients = new ClientController[0];
        return this.clientQueue.toArray(clients);
    }

    public int getQueueSize() {
        return this.clientQueue.size();
    }
}
