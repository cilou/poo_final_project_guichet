package poo_final.data;

import java.time.LocalDateTime;

public class Client {
    private int id;
    private LocalDateTime timeOfArrival;
    private ClientStatus status;

    public Client(int id) {
        this.setTimeOfArrival(LocalDateTime.now());
        this.setStatus(ClientStatus.WAITING);
        this.setId(id);
    }

    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getTimeOfArrival() {
        return this.timeOfArrival;
    }

    private void setTimeOfArrival(LocalDateTime timeOfArrival) {
        this.timeOfArrival = timeOfArrival;
    }

    public ClientStatus getStatus() {
        return status;
    }

    public void setStatus(ClientStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return String.format(
                "Client ID: %d\n" + "ClientStatus: %s\n" + "Client Waiting Time: %s\n",
                this.getId(),
                this.getStatus(),
                this.getTimeOfArrival().toString()
        );
    }
}
