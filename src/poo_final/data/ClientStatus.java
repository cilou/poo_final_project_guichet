package poo_final.data;

public enum ClientStatus {
    WAITING,
    BEING_SERVED,
    BEING_CALLED,
    LEAVING
}
