package poo_final.data;

public enum DeskStatus {
    OPEN,
    CLOSED,
    WAITING_FOR_CLIENT,
    SERVING_CLIENT
}
