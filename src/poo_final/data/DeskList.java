package poo_final.data;

import poo_final.business_logic.DeskController;

import java.util.ArrayList;

public class DeskList {

    private ArrayList<DeskController> deskList;


    public DeskList() {
        this.deskList = new ArrayList<>();
    }

    public void addDesk(DeskController desk) {
        this.deskList.add(desk);
    }

    public void removeDesk(DeskController desk) {
        this.deskList.remove(desk);
    }

    public int getSize() {
        return this.deskList.size();
    }

    public DeskController[] getDesks() {
        DeskController[] desks = new DeskController[0];
        return this.deskList.toArray(desks);
    }
}
