package poo_final.data;

import poo_final.business_logic.DisplayController;

import java.util.ArrayList;
import java.util.List;

public class DisplayList {
    List<DisplayController> displayList;

    public DisplayList() {
        this.displayList = new ArrayList<>();
    }

    public void addDisplay(DisplayController display) {
        this.displayList.add(display);
    }

    public void removeDisplay(DisplayController display) {
        this.displayList.remove(display);
    }

    public int getSize() {
        return this.displayList.size();
    }

    public DisplayController[] getDisplays() {
        DisplayController[] displays = new DisplayController[0];
        return this.displayList.toArray(displays);
    }
}
