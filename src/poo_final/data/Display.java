package poo_final.data;

import poo_final.business_logic.DeskListController;

public class Display {
    private DeskListController deskListController;
    private DisplayStatus status;

    public Display(DeskListController deskListController) {
        this.setDeskListController(deskListController);
        this.setStatus(DisplayStatus.ON);
    }

    private void setDeskListController(DeskListController deskListController) {
        this.deskListController = deskListController;
    }

    public DeskListController getDeskListController() {
        return this.deskListController;
    }

    public void setStatus(DisplayStatus status) {
        this.status = status;
    }

    public DisplayStatus getStatus() {
        return this.status;
    }
}
