package poo_final;

import poo_final.business_logic.*;
import poo_final.data.*;
import poo_final.exceptions.TicketStationException;
import poo_final.presentation.gui.*;

import java.awt.*;

public class Main {

    public static void main(String[] args) throws TicketStationException, InterruptedException {
        //consoleTests();
        guiTests();
    }

    private static void guiTests() {
        // ********
        // INIT
        // ********
        TicketStationController ticketStationController = new TicketStationController(new TicketStation());
        ClientQueueController clientQueueController = new ClientQueueController(new ClientQueue());

        // ClientQueueController will listen for events from TicketStationController (new ticket)
        ticketStationController.addTicketStationListener(clientQueueController);

        // Creation of the desks and displays lists
        DeskListController deskListController = new DeskListController(new DeskList());
        // we could use different desklist for different controllers, or same desklist for diff display ctrls
        DisplayController displayController1 = new DisplayController(new Display(deskListController));
        DisplayListController displayListController = new DisplayListController(new DisplayList());
        displayListController.addDisplay(displayController1);
        // the display will listen to events from the desk list
        deskListController.addDeskListListener(displayListController);

        // The ticket station will listen for events from desklist controller (switch ON and
        // OFF is list of desks is empty)
        deskListController.addDeskListListener(ticketStationController); // listen for list > 0 to be switched on or off
        // The desklist controller will listen for events from clientqueue controller to react
        // when a new client arrives (check if there is an open desk, ...)
        clientQueueController.addClientQueueListener(deskListController);
        // The clientqueue controller will listen to events from the desklist controller (new desk, ...)
        deskListController.addDeskListListener(clientQueueController);

        // ********
        // INIT GUI
        // ********
        EventQueue.invokeLater( () -> {
            TicketStationWindow ticketStationWindow = null;
            DeskWindow deskWindow1 = null;
            DeskWindow deskWindow2 = null;
            DeskWindow deskWindow3 = null;
            DisplayWindow displayWindow1 = null;

            try {
                ticketStationWindow = new TicketStationWindow(ticketStationController, deskListController,
                        clientQueueController);
                displayWindow1 = new DisplayWindow(displayController1, deskListController);
                //deskListController.addDeskListListener(ticketStationController);
                DeskController deskController1 = new DeskController(new Desk(), deskListController);
                deskWindow1 = new DeskWindow(deskController1);
                deskListController.addDesk(deskController1);

                DeskController deskController2 = new DeskController(new Desk(), deskListController);
                deskWindow2 = new DeskWindow(deskController2);
                deskListController.addDesk(deskController2);

                DeskController deskController3 = new DeskController(new Desk(), deskListController);
                deskWindow3 = new DeskWindow(deskController3);
                deskListController.addDesk(deskController3);

                deskWindow1.setTitle("Guichet");
                deskWindow1.setVisible(true);
                deskWindow2.setTitle("Guichet");
                deskWindow2.setVisible(true);
                deskWindow3.setTitle("Guichet");
                deskWindow3.setVisible(true);

                ticketStationWindow.setTitle("Distributeur");
                ticketStationWindow.setVisible(true);

                displayWindow1.setTitle("Afficheur 1");
                displayWindow1.setVisible(true);

            } catch(Exception ex) {
                ex.printStackTrace();
            }
        });
    }

    private static void consoleTests() throws TicketStationException, InterruptedException {
        // Initialization TicketStation and ClientQueue
        TicketStationController ticketStationController = new TicketStationController(new TicketStation());
        ClientQueueController clientQueueController = new ClientQueueController(new ClientQueue());

        // ClientQueueController will listen for events from TicketStationController (new ticket)
        ticketStationController.addTicketStationListener(clientQueueController);
        //ticketStationController.switchTicketStationOn();

        // Creation of the desks list
        DeskListController deskListController = new DeskListController(new DeskList());
        DisplayController displayController1 = new DisplayController(new Display(deskListController)); // we could use different desklist for different controllers
        DisplayListController displayListController = new DisplayListController(new DisplayList());
        displayListController.addDisplay(displayController1);
        deskListController.addDeskListListener(displayListController);
        // The ticket station will listen for events from desklist conreoller (switch ON and
        // OFF is list of desks is empty)
        deskListController.addDeskListListener(ticketStationController); // listen for list > 0 to be switched on or off
        // The desklist controller will listen for events from clientqueue controller to react
        // when a new client arrives (check if there is an open desk, ...)
        clientQueueController.addClientQueueListener(deskListController);
        // The clientqueue controller will listen to events from the desklist controller (new desk, ...)
        deskListController.addDeskListListener(clientQueueController);

        // We add a desk to the list and this switches ticketStation on since the list is no longer empty
        // desklist controller is added in the constructor to allow the list to listen to events
        // from the deskcontrollers (status changes => if OPEN then check queue and assign client)
        System.out.println();
        System.out.println("-------------------0. NEW OPEN DESK ------------------------------------------------");
        System.out.println();
        DeskController deskController1 = new DeskController(new Desk(), deskListController);
        //deskController1.addDeskListener(deskListController);
        deskListController.addDesk(deskController1);
        deskController1.setDeskStatus(DeskStatus.OPEN);

        System.out.println();
        System.out.println("-------------------1. NEW TICKET ------------------------------------------------");
        System.out.println();
        ticketStationController.newTicket();
        Thread.sleep(1000);
        System.out.println("Queue size: " + clientQueueController.getNumberOfClients());
        System.out.println("Last Ticket ID Issued: " + ticketStationController.getLastTicketIdIssued());

        System.out.println();
        System.out.println("-------------------2. NEW TICKET ------------------------------------------------");
        System.out.println();
        ticketStationController.newTicket();
        Thread.sleep(1000);
        System.out.println("Queue size: " + clientQueueController.getNumberOfClients());
        System.out.println("Last Ticket ID Issued: " + ticketStationController.getLastTicketIdIssued());

        System.out.println();
        System.out.println("-------------------3. NEW OPEN DESK ------------------------------------------------");
        System.out.println();
        DeskController deskController2 = new DeskController(new Desk(), deskListController);
        deskListController.addDesk(deskController2);
        deskController2.setDeskStatus(DeskStatus.OPEN);

        System.out.println();
        System.out.println("-------------------4. NEW TICKET ------------------------------------------------");
        System.out.println();
        ticketStationController.newTicket();
        Thread.sleep(1000);
        System.out.println("Queue size: " + clientQueueController.getNumberOfClients());
        System.out.println("Last Ticket ID Issued: " + ticketStationController.getLastTicketIdIssued());

        System.out.println();
        System.out.println("-------------------5. NEW OPEN DESK ------------------------------------------------");
        System.out.println();
        DeskController deskController3 = new DeskController(new Desk(), deskListController);
        deskController3.setDeskStatus(DeskStatus.OPEN);
        deskListController.addDesk(deskController3);

        System.out.println();
        System.out.println("-------------------6. NEW TICKET ------------------------------------------------");
        System.out.println();
        ticketStationController.newTicket();
        Thread.sleep(1000);
        System.out.println("Queue size: " + clientQueueController.getNumberOfClients());
        System.out.println("Last Ticket ID Issued: " + ticketStationController.getLastTicketIdIssued());

        System.out.println();
        System.out.println("-------------------7. NEW CLOSED DESK ------------------------------------------------");
        System.out.println();
        DeskController deskController4 = new DeskController(new Desk(), deskListController);
        deskController4.setDeskStatus(DeskStatus.CLOSED);
        deskListController.addDesk(deskController4);

        System.out.println();
        System.out.println("--------------------8. OPENING LAST DESK-----------------------------------------------");
        System.out.println();
        deskController4.setDeskStatus(DeskStatus.OPEN);

        System.out.println();
        System.out.println("--------------------CLIENT QUEUE STATUS-----------------------------------");
        System.out.println();
        for(ClientController client : clientQueueController.getClientList()) {
            System.out.println(client);
        }
    }
}
