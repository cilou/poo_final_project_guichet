package poo_final.presentation.interfaces;

import poo_final.business_logic.ClientController;

import java.util.EventListener;

public interface IClientQueueListener extends EventListener {

    public void onNewClient(ClientController client);
}
