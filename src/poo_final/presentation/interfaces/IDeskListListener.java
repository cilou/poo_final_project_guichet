package poo_final.presentation.interfaces;

import poo_final.business_logic.DeskController;
import poo_final.data.Desk;

import java.util.EventListener;

public interface IDeskListListener extends EventListener {
    public void onDeskListChange(DeskController desk, int oldCount, int newCount);
    public void onOpenDeskAvailable(DeskController desk);
}
