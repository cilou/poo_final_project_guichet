package poo_final.presentation.interfaces;

import poo_final.business_logic.ClientController;

import java.util.EventListener;

public interface IClientListener extends EventListener {
    public void onClientStatusChanged(ClientController clientController);
}
