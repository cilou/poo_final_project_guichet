package poo_final.presentation.interfaces;

import java.util.EventListener;

public interface ITicketStationListener extends EventListener {
    public void onNewTicket(int ticketId);
}
