package poo_final.presentation.interfaces;

import poo_final.business_logic.DeskController;

import java.util.EventListener;

public interface IDeskListener extends EventListener {
    public void onDeskStatusChanged(DeskController desk);
}
