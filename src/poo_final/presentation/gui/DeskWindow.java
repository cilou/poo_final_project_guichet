package poo_final.presentation.gui;

import poo_final.business_logic.ClientController;
import poo_final.business_logic.DeskController;
import poo_final.data.ClientStatus;
import poo_final.data.DeskStatus;
import poo_final.presentation.interfaces.IClientListener;
import poo_final.presentation.interfaces.IDeskListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DeskWindow extends JFrame implements IDeskListener/*, IClientListener*/ {

    private DeskController deskController;

    private JLabel lblDeskId = new JLabel();
    private JLabel lblClentId = new JLabel("On sert le ---");
    private JCheckBox chkIsDeskClosed = new JCheckBox("En pause");
    private JButton btnNextClient = new JButton("Client suivant");

    public DeskWindow(DeskController deskController) {
        this.deskController = deskController;
        this.deskController.addDeskListener(this); // we listen to the desk status changes

        initComponents();

        onDeskStatusChanged(this.deskController); // triggers background coloring
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setResizable(false);
    }

    private void initComponents() {
        lblDeskId.setFont(new Font("Arial", Font.PLAIN, 15));
        lblClentId.setFont(new Font("Arial", Font.PLAIN, 15));
        lblDeskId.setText("Guichet " + deskController.getDeskId());

        Container contentPane = this.getContentPane();
        GroupLayout groupLayout = new GroupLayout(contentPane);
        contentPane.setLayout(groupLayout);

        chkIsDeskClosed.addActionListener(new ChangeDeskClosedStatus());
        btnNextClient.addActionListener(new NextClient());

        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup()
                .addGroup(groupLayout.createSequentialGroup()
                        .addComponent(lblDeskId)
                        .addComponent(chkIsDeskClosed)
                )
                .addGap(10)
                .addGroup(groupLayout.createSequentialGroup()
                        .addComponent(lblClentId)
                        .addComponent(btnNextClient)
                )
        );

        groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
                .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(lblDeskId)
                        .addComponent(chkIsDeskClosed)
                )
                .addGap(10)
                .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(lblClentId)
                        .addComponent(btnNextClient)
                )
        );

        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        groupLayout.linkSize(SwingConstants.HORIZONTAL, lblClentId, lblDeskId, chkIsDeskClosed, btnNextClient);
        chkIsDeskClosed.setSelected(deskController.getDeskStatus() == DeskStatus.CLOSED);
        pack();
    }

    // ======================
    // Gestion des événements
    // ======================
    @Override
    public void onDeskStatusChanged(DeskController desk) {

        System.out.println("==> DeskWindow -- onDeskStatusChanged -- desk id " + desk.getDeskId() +
                ", status " + desk.getDeskStatus());
        if(desk.getDeskId() != deskController.getDeskId()) return;

        if(desk.getDeskStatus() == DeskStatus.WAITING_FOR_CLIENT) {
            getContentPane().setBackground(Color.YELLOW);
            chkIsDeskClosed.setEnabled(false);
            lblClentId.setText("On attend le client " + desk.getClient().getClientId());
            btnNextClient.setEnabled(true);
        } else if(desk.getDeskStatus() == DeskStatus.SERVING_CLIENT) {
            getContentPane().setBackground(Color.GREEN);
            chkIsDeskClosed.setEnabled(true);
            lblClentId.setText("On sert le client " + desk.getClient().getClientId());
            btnNextClient.setEnabled(true);
        } else if(desk.getDeskStatus() == DeskStatus.OPEN) {
            getContentPane().setBackground(Color.PINK);
            chkIsDeskClosed.setEnabled(true);
            lblClentId.setText("On sert le ---");
            btnNextClient.setEnabled(false);
        } else if(desk.getDeskStatus() == DeskStatus.CLOSED) {
            getContentPane().setBackground(Color.RED);
            chkIsDeskClosed.setEnabled(true);
            lblClentId.setText("On sert le ---");
            btnNextClient.setEnabled(false);
            if(deskController.getClient() != null) {
                deskController.getClient().setClientStatus(ClientStatus.LEAVING);
            }
        } else {
            getContentPane().setBackground(Color.LIGHT_GRAY);
            chkIsDeskClosed.setEnabled(false);
            lblClentId.setText("On sert le ---");
            btnNextClient.setEnabled(false);
        }
    }

    // ======================
    // Gestion des actions
    // ======================
    private class ChangeDeskClosedStatus implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JCheckBox checkBox = (JCheckBox) e.getSource();
            if(checkBox.isSelected()) {
                deskController.setDeskStatus(DeskStatus.CLOSED);
            } else {
                deskController.setDeskStatus(DeskStatus.OPEN);
            }
        }
    }

    private class NextClient implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if(deskController.getClient() != null) {
                ClientController currentClient = deskController.getClient();
                // If we leave the currentListener, the status OPEN for the desk will be set twice
                currentClient.removeClientListener(deskController);
                currentClient.setClientStatus(ClientStatus.LEAVING); // we close the window
                deskController.setDeskStatus(DeskStatus.OPEN); // we call the next client if there is any
            } else {
                deskController.setDeskStatus(DeskStatus.OPEN);
            }
        }
    }
}
