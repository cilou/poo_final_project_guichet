package poo_final.presentation.gui;

import poo_final.business_logic.DeskController;
import poo_final.business_logic.DeskListController;
import poo_final.business_logic.DisplayController;
import poo_final.presentation.interfaces.IDeskListListener;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DisplayWindow extends JFrame implements IDeskListListener {

    private final int THREAD_CURRENT_TIME_INTERVAL_MS = 1000;

    private DisplayController displayController;
    private LocalDateTime currentTime;
    private ThreadCurrentTime threadCurrentTime;

    private JLabel lblCurrentTime = new JLabel();
    private List<DeskController> listDesks = new ArrayList<>();
    private JTextArea txtAreaDesks = new JTextArea();

    public DisplayWindow(DisplayController displayController, DeskListController deskListController) {
        this.displayController = displayController;
        deskListController.addDeskListListener(this);

        initComponents();
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setResizable(false);

        threadCurrentTime = new ThreadCurrentTime();
        threadCurrentTime.start();
    }

    private void initComponents() {
        lblCurrentTime.setFont(new Font("Arial", Font.PLAIN, 25));
        txtAreaDesks.setRows(7);
        txtAreaDesks.setColumns(40);
        txtAreaDesks.setEditable(false);
        txtAreaDesks.setEnabled(false);
        txtAreaDesks.setBackground(Color.DARK_GRAY);

        Container contentPane = this.getContentPane();
        GroupLayout groupLayout = new GroupLayout(contentPane);
        contentPane.setLayout(groupLayout);

        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup()
                .addComponent(lblCurrentTime)
                .addComponent(txtAreaDesks)
        );

        groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
                .addComponent(lblCurrentTime)
                .addComponent(txtAreaDesks)
        );

        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        pack();
    }

    private void addDesk(DeskController desk) {
        this.listDesks.add(desk);
    }

    private void removeDesk(DeskController desk) {
        this.listDesks.remove(desk);
    }

    private void updateDeskStatus() {
        txtAreaDesks.setText("");
        for(DeskController desk : this.listDesks) {
            String str1 = String.format(" %-25s", String.format("Desk ID: %d", desk.getDeskId()));
            String str2 = String.format(" %-40s\t", String.format("STATUS: %-20s", desk.getDeskStatus()));
            String strClient = "";
            if(desk.getClient() == null) {
                strClient = String.format("%-10s", "CLIENT: ---");
            } else {
                strClient = String.format("%-10s", String.format("CLIENT: %d", desk.getClient().getClient().getId()));
            }
            String str = str1 + str2 + strClient;
            txtAreaDesks.setText(txtAreaDesks.getText() + "\n" + str);
        }
    }

    private void updateTime() {
        String time = String.format("%02d:%02d:%02d",
                currentTime.getHour(), currentTime.getMinute(), currentTime.getSecond());
        this.lblCurrentTime.setText(time);
    }

    // ======================
    // Gestion des événements
    // ======================
    @Override
    public void onDeskListChange(DeskController desk, int oldCount, int newCount) {
        System.out.println("==> DisplayWindow -- received signal onDeskListChange -- oldCount: " + oldCount +
                " newCount: " + newCount + " desk id: " + desk.getDeskId() + " list size: " + listDesks.size());
        if(oldCount > newCount) {
            this.removeDesk(desk);
        } else if(oldCount < newCount){
            this.addDesk(desk);
        }
        this.updateDeskStatus();
    }

    // UNUSED
    @Override
    public void onOpenDeskAvailable(DeskController desk) {}

    // ====================
    // Gestion des threads
    // ====================
    private class ThreadCurrentTime implements Runnable {

        @Override
        public void run() {
            while(!Thread.currentThread().isInterrupted()) {
                try {
                    currentTime = LocalDateTime.now();
                    updateTime();
                    Thread.sleep(THREAD_CURRENT_TIME_INTERVAL_MS);
                } catch(InterruptedException ex) {
                    System.out.println("DisplayWindow -- ThreadCurrentTime interrupted");
                }
            }
        }

        public void start() {
            Thread t = new Thread(this);
            t.start();
        }
    }
}
