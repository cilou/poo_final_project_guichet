package poo_final.presentation.gui;

import poo_final.business_logic.ClientController;
import poo_final.data.ClientStatus;
import poo_final.presentation.interfaces.IClientListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class ClientWindow extends JFrame implements IClientListener {

    private final int THREAD_WAIT_TIME_INTERVAL_MS = 1000;

    private ClientController clientController;
    private ThreadWaitTime threadWaitTime;
    private boolean isClientWaiting;

    private JLabel lblWaitTime = new JLabel();
    private JLabel lblClientId = new JLabel();
    private JButton btnGoToDesk = new JButton();
    private JButton btnLeave = new JButton();

    public ClientWindow(ClientController clientController) {
        this.clientController = clientController;
        this.clientController.addClientListener(this);
        initComponents();

        isClientWaiting = true;
        this.clientController.setClientStatus(ClientStatus.WAITING);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);

        threadWaitTime = new ThreadWaitTime();
        threadWaitTime.start();
    }

    private void initComponents() {
        lblWaitTime.setText(clientController.getTimeElapsedSinceArrivalFormatted());
        lblClientId.setText(Integer.toString(clientController.getClientId()));
        lblClientId.setFont(new Font("Arial", Font.PLAIN, 40));
        btnGoToDesk.setText("Aller au guichet X");
        btnGoToDesk.addActionListener(new GoToDesk());
        btnLeave.setText("Partir");
        btnLeave.addActionListener(new Leave());

        Container contentPane = this.getContentPane();
        GroupLayout groupLayout = new GroupLayout(contentPane);
        contentPane.setLayout(groupLayout);

        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addComponent(lblWaitTime)
                )
                .addComponent(lblClientId)
                .addGroup(groupLayout.createSequentialGroup()
                        .addComponent(btnGoToDesk)
                        .addComponent(btnLeave)
                )
        );

        groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
                .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(lblWaitTime)
                )
                .addComponent(lblClientId)
                .addGroup(groupLayout.createParallelGroup()
                        .addComponent(btnGoToDesk)
                        .addComponent(btnLeave)
                )
        );

        groupLayout.linkSize(btnGoToDesk, btnLeave);
        groupLayout.setAutoCreateContainerGaps(true);
        groupLayout.setAutoCreateGaps(true);
        pack();
    }

    // ======================
    // Gestion des événements
    // ======================
    @Override
    public void onClientStatusChanged(ClientController clientController) {
        System.out.println("ClientWindow -- received signal onClientStatusChanged from ClientController");
        if(clientController.getClientStatus() == ClientStatus.BEING_SERVED) {
            getContentPane().setBackground(Color.GREEN);
            btnGoToDesk.setEnabled(false);
            btnLeave.setEnabled(false);
            btnGoToDesk.setText("Guichet " + clientController.getDeskId());
        } else if(clientController.getClientStatus() == ClientStatus.BEING_CALLED) {
            isClientWaiting = false;
            getContentPane().setBackground(Color.PINK);
            btnGoToDesk.setEnabled(true);
            btnLeave.setEnabled(true);
            btnGoToDesk.setText("Aller au guichet " + clientController.getDeskId());
        } else if(clientController.getClientStatus() == ClientStatus.WAITING) {
            getContentPane().setBackground(Color.LIGHT_GRAY);
            btnGoToDesk.setEnabled(false);
            btnLeave.setEnabled(true);
            btnGoToDesk.setText("Aller au guichet X");
        } else if(clientController.getClientStatus() == ClientStatus.LEAVING) {
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        }
    }

    // ======================
    // Gestion des actions
    // ======================
    private class GoToDesk implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            clientController.setClientStatus(ClientStatus.BEING_SERVED);
        }
    }

    private class Leave implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            clientController.setClientStatus(ClientStatus.LEAVING);
        }
    }

    // ====================
    // Gestion des threads
    // ====================
    private class ThreadWaitTime implements Runnable {

        @Override
        public void run() {
            while(isClientWaiting && !Thread.currentThread().isInterrupted()) {
                try {
                    lblWaitTime.setText(clientController.getTimeElapsedSinceArrivalFormatted());
                    Thread.sleep(THREAD_WAIT_TIME_INTERVAL_MS);
                } catch(InterruptedException ex) {
                    System.out.println("ClientWindow -- ThreadWaitTime interrupted");
                }
            }
        }

        public void start() {
            Thread t = new Thread(this);
            t.start();
        }
    }
}
