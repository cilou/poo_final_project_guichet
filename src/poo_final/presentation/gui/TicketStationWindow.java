package poo_final.presentation.gui;

import poo_final.business_logic.*;
import poo_final.data.TicketStationStatus;
import poo_final.exceptions.TicketStationException;
import poo_final.presentation.interfaces.IClientQueueListener;
import poo_final.presentation.interfaces.IDeskListListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TicketStationWindow extends JFrame implements IDeskListListener, IClientQueueListener {
    private TicketStationController ticketStationController;
    private JButton btnTakeATicket = new JButton("Prendre un ticket");
    private JLabel lblMessage = new JLabel();

    public TicketStationWindow(TicketStationController ticketStationController, DeskListController deskListController,
                               ClientQueueController clientQueueController) {
        this.ticketStationController = ticketStationController;
        deskListController.addDeskListListener(this);
        clientQueueController.addClientQueueListener(this);

        btnTakeATicket.setEnabled(false);

        initComponents();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
    }

    private void initComponents() {
        Container paneContainer = this.getContentPane();
        GroupLayout groupLayout = new GroupLayout(paneContainer);
        paneContainer.setLayout(groupLayout);

        btnTakeATicket.setFont(new Font("Arial", Font.PLAIN, 25));
        btnTakeATicket.addActionListener(new GenerateNewTicket());

        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
            .addComponent(btnTakeATicket)
            .addComponent(lblMessage)
        );

        groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
                .addComponent(btnTakeATicket)
                .addComponent(lblMessage)
        );

        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        pack();
    }

    // ===================
    // Gestion des actions
    // ===================
    private class GenerateNewTicket implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                ticketStationController.newTicket();
                //lblMessage.setText("Le ticket a été créé.");
            } catch(TicketStationException ex) {
                lblMessage.setText("Le ticket n'a pas pu être créé.");
                System.out.println(ex.getMessage());
            }
        }
    }

    // ======================
    // Gestion des événements
    // ======================
    @Override
    public void onDeskListChange(DeskController desk, int oldCount, int newCount) {
        this.btnTakeATicket.setEnabled(ticketStationController.getSTicketStationStatus() == TicketStationStatus.ON);
    }

    // UNUSED
    @Override
    public void onOpenDeskAvailable(DeskController desk) {

    }

    @Override
    public void onNewClient(ClientController client) {
        ClientWindow clientWindow = new ClientWindow(client);
        clientWindow.setTitle("Client");
        clientWindow.setVisible(true);
    }
}
