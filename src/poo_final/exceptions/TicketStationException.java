package poo_final.exceptions;

public class TicketStationException extends Exception {

    private int code;

    public TicketStationException(String message) {
        super(message);
        this.setCode(0);
    }

    public TicketStationException(String message, int code) {
        super(message);
        this.setCode(code);
    }

    public int getCode() {
        return code;
    }

    private void setCode(int code) {
        this.code = code;
    }

}
